package com.cursoandroid.restaurant.ui.view.login

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import com.cursoandroid.restaurant.R
import com.cursoandroid.restaurant.databinding.ActivityLoginBinding
import com.cursoandroid.restaurant.model.interfaces.ListenerLogin
import com.cursoandroid.restaurant.ui.utils.Constants
import com.cursoandroid.restaurant.ui.view.home.MenuActivity
import com.cursoandroid.restaurant.ui.viewmodel.login.VMLogin
import com.cursoandroid.restaurant.ui.viewutils.toast
import com.google.firebase.auth.FirebaseAuth

class LoginActivity : AppCompatActivity(), ListenerLogin {

    private lateinit var binding: ActivityLoginBinding
    private lateinit var viewModel: VMLogin

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = DataBindingUtil.setContentView(this,R.layout.activity_login)
        viewModel = ViewModelProvider(this).get(VMLogin::class.java)

        binding.vmLogin = viewModel
        viewModel.listener = this
    }

    override fun onVerifyLogin(user: String, password: String) {
        FirebaseAuth.getInstance().signInWithEmailAndPassword(user,password)
            .addOnCompleteListener { task ->
                if (task.isSuccessful) {
                    val menuIntent = Intent(this, MenuActivity::class.java)
                    startActivity(menuIntent)
                    finish()
                }else{
                    toast(getString(R.string.st_error_login))
                }
            }
    }

    override fun onErrorLogin(errorType: Int) {
        when(errorType){
            Constants.ERROR_EMPTY_EMAIL -> toast(getString(R.string.st_empty_user))
            Constants.ERROR_EMPTY_PASSWORD -> toast(getString(R.string.st_empty_pass))
        }
    }

    override fun onForgetLogin() {
        toast("Usuario: ${Constants.USER} \r\nConstraseña: ${Constants.PASS}")
    }
}