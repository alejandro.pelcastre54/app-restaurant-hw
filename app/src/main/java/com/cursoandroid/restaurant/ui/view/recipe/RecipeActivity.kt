package com.cursoandroid.restaurant.ui.view.recipe

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.widget.Button
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import com.cursoandroid.restaurant.R
import com.cursoandroid.restaurant.databinding.ActivityRecipeBinding
import com.cursoandroid.restaurant.model.interfaces.RecipeListener
import com.cursoandroid.restaurant.ui.utils.Constants
import com.cursoandroid.restaurant.ui.viewmodel.recipe.RecipeVM
import kotlinx.android.synthetic.main.activity_recipe.*

class RecipeActivity : AppCompatActivity(), RecipeListener {

    private lateinit var binding: ActivityRecipeBinding
    private lateinit var viewModel: RecipeVM

    // Data Received
    private var title: String? = null
    private var tag: String? = null
    private var ingredient: String? = ""
    private var portion: String?= ""
    private var instruction: String? = null
    private var videoURL: String? = null
    private var sourceURL: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this,R.layout.activity_recipe)
        viewModel = ViewModelProvider(this).get(RecipeVM::class.java)
        binding.vmRecipe = viewModel
        viewModel.listener = this

        getData()

    }

    private fun getData(){
        title = intent.getStringExtra(Constants.NAME)
        tag = intent.getStringExtra(Constants.TAG)
        instruction = intent.getStringExtra(Constants.INSTRUCTION)
        videoURL = intent.getStringExtra(Constants.VIDEO)
        sourceURL = intent.getStringExtra(Constants.SOURCE)

        for (number in 1..16) {
            val ingredientNumber = number.toString()
            if (!intent.getStringExtra("${Constants.INGREDIENT}${ingredientNumber}").isNullOrEmpty()) {
                ingredient += intent.getStringExtra("${Constants.INGREDIENT}${ingredientNumber}") + "\r\n"
                portion += intent.getStringExtra("${Constants.PORTION}${ingredientNumber}") + "\r\n"
            }
        }
        showData(title,tag,instruction,sourceURL,ingredient,portion)
    }

    private fun showData(
        title:String?,
        tag:String?,
        instruction:String?,
        sourceURL:String?,
        ingredient:String?,
        portion:String?
    ){
        binding.apply {
            tvTitleRecipe.text = title
            tvTitleTag.text = tag
            tvTitleIngredient.text = ingredient
            tvTitlePortion.text = portion
            tvTitleInstructionsBody.text = instruction
            tvTitleSource.text = sourceURL
        }
    }

    override fun onButtonYoutube() {
        var youTubeIntent = Intent(Intent.ACTION_VIEW,Uri.parse(videoURL))
        startActivity(youTubeIntent)
    }
}