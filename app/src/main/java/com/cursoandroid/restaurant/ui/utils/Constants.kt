package com.cursoandroid.restaurant.ui.utils

object Constants {

    val ERROR_EMPTY_EMAIL = 1
    val ERROR_EMPTY_PASSWORD = 2

    val USER = "alex@correo.com"
    val PASS = "ale123"

    val NAME = "nombre"
    val TAG = "tag"
    val INGREDIENT = "ingrediente"
    val PORTION = "portion"
    val ONE = "1"
    val TWO = "2"
    val THREE = "3"
    val FOUR = "4"
    val FIVE = "5"
    val SIX = "6"
    val SEVEN = "7"
    val EIGHT = "8"
    val NINE = "9"
    val TEN = "10"
    val ELEVEN = "11"
    val TWELVE = "12"
    val THIRTEEN = "13"
    val FOURTEEN = "14"
    val FIFTEEN = "15"
    val SIXTEEN = "16"
    val INSTRUCTION = "instruccion"
    val VIDEO = "video"
    val SOURCE = "fuente"

}