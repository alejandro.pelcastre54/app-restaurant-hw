package com.cursoandroid.restaurant.ui.viewmodel.home

import android.app.Application
import android.view.View
import androidx.lifecycle.AndroidViewModel
import com.cursoandroid.restaurant.data.network.repository.MealsRecipeRepository
import com.cursoandroid.restaurant.model.interfaces.MenuListener
import com.cursoandroid.restaurant.ui.utils.CoroutinesItems

class MenuVM(application: Application):AndroidViewModel(application) {

    var listener:MenuListener?= null
    fun onDownloadValue(view: View){
        getItems("")
    }

    private fun getItems(value:String){
        CoroutinesItems.main {
            val response = MealsRecipeRepository().getMeals()
            if (response.isSuccessful){
                listener?.onSuccess(response.body()?.meals!!)
            }else{
                listener?.onError(1)
            }
        }
    }

}