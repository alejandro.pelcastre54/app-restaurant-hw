package com.cursoandroid.restaurant.ui.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.cursoandroid.restaurant.R
import com.cursoandroid.restaurant.data.network.models.response.Meal
import com.cursoandroid.restaurant.model.interfaces.MenuListener

class MealsAdapter (var itemList :List<Meal>, var clickLister:MenuListener): RecyclerView.Adapter<MealsAdapter.ItemsViewHolder>() {

    class ItemsViewHolder(view: View):RecyclerView.ViewHolder(view){
        private val tvName = view.findViewById<TextView>(R.id.tv_title_meal)
        private val tvValue = view.findViewById<TextView>(R.id.tv_category_meal)
        private val tvArea = view.findViewById<TextView>(R.id.tv_area_meal)
        private val imgItem = view.findViewById<ImageView>(R.id.img_meal)

        fun bind(item:Meal, action:MenuListener){
            tvName.text = item.strMeal
            tvValue.text = item.strCategory
            tvArea.text = item.strArea


            Glide.with(imgItem.context).load(item.strMealThumb).into(imgItem)

            itemView.setOnClickListener {
                action.itemClicked(item, adapterPosition)
            }
        }

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemsViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        return ItemsViewHolder(inflater.inflate(R.layout.item_meals, parent, false))
    }

    override fun onBindViewHolder(holder: ItemsViewHolder, position: Int) {
        holder.bind(itemList[position],clickLister)
    }

    override fun getItemCount(): Int = itemList.size

}