package com.cursoandroid.restaurant.ui.viewmodel.recipe

import android.app.Application
import android.view.View
import androidx.lifecycle.AndroidViewModel
import com.cursoandroid.restaurant.model.interfaces.RecipeListener

class RecipeVM (application:Application):AndroidViewModel(application) {

    var listener:RecipeListener?= null

    fun openYoutube(view: View){
        listener?.onButtonYoutube()
    }

}