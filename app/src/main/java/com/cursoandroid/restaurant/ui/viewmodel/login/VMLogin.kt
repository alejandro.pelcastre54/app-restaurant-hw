package com.cursoandroid.restaurant.ui.viewmodel.login

import android.app.Application
import android.view.View
import androidx.databinding.ObservableField
import androidx.lifecycle.AndroidViewModel
import com.cursoandroid.restaurant.model.interfaces.ListenerLogin
import com.cursoandroid.restaurant.ui.utils.Constants

class VMLogin (application: Application):AndroidViewModel(application){

    var listener:ListenerLogin?= null
    val user= ObservableField<String>("")
    val pass= ObservableField<String>("")

    fun validateFields (view: View){
        if(!user.get().isNullOrEmpty()&&!pass.get().isNullOrEmpty()){
            listener?.onVerifyLogin(user.get().toString(),pass.get().toString())
        }else if(user.get().isNullOrEmpty()){
            listener?.onErrorLogin(Constants.ERROR_EMPTY_EMAIL)
        }else if(pass.get().isNullOrEmpty()){
            listener?.onErrorLogin(Constants.ERROR_EMPTY_PASSWORD)
        }

    }

    fun forgetCredentials(view: View){
        listener?.onForgetLogin()
    }
}