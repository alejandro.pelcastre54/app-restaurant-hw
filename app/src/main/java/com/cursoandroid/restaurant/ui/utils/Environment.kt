package com.cursoandroid.restaurant.ui.utils

object Environment {

    const val URL_BASE = "https://www.themealdb.com/api/json/v1/"
    const val END_POINT = "1/search.php?s="

}