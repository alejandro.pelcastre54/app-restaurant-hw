package com.cursoandroid.restaurant.ui.view.home

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.cursoandroid.restaurant.R
import com.cursoandroid.restaurant.data.network.models.response.Meal
import com.cursoandroid.restaurant.data.network.service.MealsRecipesApi
import com.cursoandroid.restaurant.databinding.ActivityMenuBinding
import com.cursoandroid.restaurant.model.interfaces.MenuListener
import com.cursoandroid.restaurant.ui.adapters.MealsAdapter
import com.cursoandroid.restaurant.ui.utils.Constants
import com.cursoandroid.restaurant.ui.view.login.LoginActivity
import com.cursoandroid.restaurant.ui.view.recipe.RecipeActivity
import com.cursoandroid.restaurant.ui.viewmodel.home.MenuVM

class MenuActivity : AppCompatActivity(), MenuListener {

    private lateinit var viewModel: MenuVM
    private lateinit var binding: ActivityMenuBinding

    private lateinit var recyclerView: RecyclerView
    private lateinit var layoutManager: LinearLayoutManager
    private lateinit var adapter: MealsAdapter


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding =DataBindingUtil.setContentView(this,R.layout.activity_menu)
        viewModel = ViewModelProvider(this).get(MenuVM::class.java)
        binding.vm = viewModel
        viewModel.listener = this

        binding.apply { recyclerView = rvMeals }
        layoutManager = LinearLayoutManager(this, RecyclerView.VERTICAL,false)
        recyclerView.layoutManager = layoutManager
    }

    override fun itemClicked(item: Meal, position: Int) {
        val recipeDetailIntent = Intent (this, RecipeActivity::class.java).apply{
            putExtra(Constants.NAME,item.strMeal)
            putExtra(Constants.TAG,item.strTags)
            putExtra("${Constants.INGREDIENT}${Constants.ONE}",item.strIngredient1)
            putExtra("${Constants.INGREDIENT}${Constants.TWO}",item.strIngredient2)
            putExtra("${Constants.INGREDIENT}${Constants.THREE}",item.strIngredient3)
            putExtra("${Constants.INGREDIENT}${Constants.FOUR}",item.strIngredient4)
            putExtra("${Constants.INGREDIENT}${Constants.FIVE}",item.strIngredient5)
            putExtra("${Constants.INGREDIENT}${Constants.SIX}",item.strIngredient6)
            putExtra("${Constants.INGREDIENT}${Constants.SEVEN}",item.strIngredient7)
            putExtra("${Constants.INGREDIENT}${Constants.EIGHT}",item.strIngredient8)
            putExtra("${Constants.INGREDIENT}${Constants.NINE}",item.strIngredient9)
            putExtra("${Constants.INGREDIENT}${Constants.TEN}",item.strIngredient10)
            putExtra("${Constants.INGREDIENT}${Constants.ELEVEN}",item.strIngredient11)
            putExtra("${Constants.INGREDIENT}${Constants.TWELVE}",item.strIngredient12)
            putExtra("${Constants.INGREDIENT}${Constants.THIRTEEN}",item.strIngredient13)
            putExtra("${Constants.INGREDIENT}${Constants.FOURTEEN}",item.strIngredient14)
            putExtra("${Constants.INGREDIENT}${Constants.FIFTEEN}",item.strIngredient15)
            putExtra("${Constants.INGREDIENT}${Constants.SIXTEEN}",item.strIngredient16)
            putExtra("${Constants.PORTION}${Constants.ONE}",item.strMeasure1)
            putExtra("${Constants.PORTION}${Constants.TWO}",item.strMeasure2)
            putExtra("${Constants.PORTION}${Constants.THREE}",item.strMeasure3)
            putExtra("${Constants.PORTION}${Constants.FOUR}",item.strMeasure4)
            putExtra("${Constants.PORTION}${Constants.FIVE}",item.strMeasure5)
            putExtra("${Constants.PORTION}${Constants.SIX}",item.strMeasure6)
            putExtra("${Constants.PORTION}${Constants.SEVEN}",item.strMeasure7)
            putExtra("${Constants.PORTION}${Constants.EIGHT}",item.strMeasure8)
            putExtra("${Constants.PORTION}${Constants.NINE}",item.strMeasure9)
            putExtra("${Constants.PORTION}${Constants.TEN}",item.strMeasure10)
            putExtra("${Constants.PORTION}${Constants.ELEVEN}",item.strMeasure11)
            putExtra("${Constants.PORTION}${Constants.TWELVE}",item.strMeasure12)
            putExtra("${Constants.PORTION}${Constants.THIRTEEN}",item.strMeasure13)
            putExtra("${Constants.PORTION}${Constants.FOURTEEN}",item.strMeasure14)
            putExtra("${Constants.PORTION}${Constants.FIFTEEN}",item.strMeasure15)
            putExtra("${Constants.PORTION}${Constants.SIXTEEN}",item.strMeasure16)
            putExtra(Constants.INSTRUCTION,item.strInstructions)
            putExtra(Constants.VIDEO,item.strYoutube)
            putExtra(Constants.SOURCE,item.strSource)

        }
        startActivity(recipeDetailIntent)
    }

    override fun onSuccess(items: List<Meal>) {
        adapter = MealsAdapter(items, this)
        recyclerView.adapter = adapter
        adapter.notifyDataSetChanged()
    }

    override fun onError(errorCode: Int) {
        TODO("Not yet implemented")
    }
}