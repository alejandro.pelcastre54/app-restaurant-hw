package com.cursoandroid.restaurant.model.interfaces

import com.cursoandroid.restaurant.data.network.models.response.Meal
import java.text.FieldPosition

interface MenuListener {
    fun itemClicked(item: Meal, position: Int)
    fun onSuccess(items:List<Meal>)
    fun onError(errorCode:Int)
}