package com.cursoandroid.restaurant.model.interfaces

interface ListenerLogin {
    fun onVerifyLogin(user:String, password:String)
    fun onErrorLogin(errorType: Int)
    fun onForgetLogin()
}