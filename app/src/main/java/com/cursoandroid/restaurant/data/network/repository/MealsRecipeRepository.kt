package com.cursoandroid.restaurant.data.network.repository

import com.cursoandroid.restaurant.data.network.models.response.MealsResponse
import com.cursoandroid.restaurant.data.network.service.MealsRecipesApi
import retrofit2.Response

class MealsRecipeRepository {

    suspend fun getMeals(): Response<MealsResponse> {
        return MealsRecipesApi().getMeals()
    }
}