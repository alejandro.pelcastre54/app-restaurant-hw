package com.cursoandroid.restaurant.data.network.models.response

data class MealsResponse(
    val meals: List<Meal>
)