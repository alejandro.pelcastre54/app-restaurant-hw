package com.cursoandroid.restaurant.data.network.service

import com.cursoandroid.restaurant.data.network.models.response.MealsResponse
import com.cursoandroid.restaurant.ui.utils.Environment
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET

interface MealsRecipesApi {

    @GET(Environment.END_POINT)
    /*fun getRecipeInfo(): Response<MealsResponse>
    abstract fun getMeals(): Response<MealsResponse>*/
    suspend fun getMeals():Response<MealsResponse>

    companion object {
        operator fun invoke(): MealsRecipesApi {
            return Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl(Environment.URL_BASE)
                .build()
                .create(MealsRecipesApi::class.java)
        }
    }
}